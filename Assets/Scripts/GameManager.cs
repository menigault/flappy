﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;
    public AudioSource music;
    public AudioSource smash;

    public uint score = 0;
    

	// Use this for initialization
	void Start () {
        if (instance == null) instance = this;
        else if(instance != this) Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

    }

    public void playerDie(GameObject player)
    {
        if(PlayerPrefs.GetInt("score", 0) < score)
        {
            PlayerPrefs.SetInt("score", (int)score);
            PlayerPrefs.Save();
        }
        smash.Play();
        SceneManager.LoadScene("Scenes/04-Over");
    }

    public void addScore()
    {
        score++;
        GameObject.FindGameObjectWithTag("score").GetComponent<Text>().text = score.ToString() ;
    }

    // Update is called once per frame

}

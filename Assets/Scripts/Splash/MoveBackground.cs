﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBackground : MonoBehaviour {

    private Vector2 startPos;
    private float tileSize;

    public float scrollSpeed = 0;
    

	// Use this for initialization
	void Start () {
        startPos = transform.position;
        tileSize = GetComponent<SpriteRenderer>().bounds.size.x;
	}
	
	// Update is called once per frame
	void Update () {
        float newPos = Mathf.Repeat(Time.time * scrollSpeed, tileSize);
        transform.position = startPos + new Vector2(1, 0) * newPos;
	}
}

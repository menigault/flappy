﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTitle : MonoBehaviour {

    public float speedY = -1;

    // Update is called once per frame
    void Update () {
        Vector2 rectPos = GetComponent<RectTransform>().position;
        Vector2 canvasPos = GameObject.Find("Canvas").GetComponent<RectTransform>().position;
	    if(rectPos.y > canvasPos.y / 3)
        {
            float newPosY = rectPos.y + speedY;
            GetComponent<RectTransform>().position = new Vector2(rectPos.x, newPosY);
        }
	}
}

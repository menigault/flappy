﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

    public float xSpeed = 0.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Rigidbody2D pipe = GetComponent<Rigidbody2D>();
		if(pipe != null)
        {
            pipe.velocity = new Vector2(xSpeed, 0);
        }
	}
}

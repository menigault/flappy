﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public float force = 2.0f;
    public float cooldown = 0.002f; // en secondes

    private float timeWithoutJump;

	// Use this for initialization
	void Start () {
        timeWithoutJump = cooldown;
	}
	
	// Update is called once per frame
	void Update () {

        timeWithoutJump += Time.deltaTime;
        if (timeWithoutJump >= cooldown)
        {

#if UNITY_ANDROID
            for (int i = 0; i < Input.touchCount; ++i)
            {
                if (Input.GetTouch(i).phase == TouchPhase.Began) {
                    timeWithoutJump = 0.0f;
                    GetComponent<Rigidbody2D>().velocity = new Vector2(0, force);
                }
            }
#endif
#if UNITY_STANDALONE || UNITY_EDITOR
            if (Input.GetAxis("Jump") > 0)
            {
                timeWithoutJump = 0.0f;
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, force);
            }
#endif
        }

    }
}

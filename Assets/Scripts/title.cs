﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class title : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameManager.instance.score = 0;
        int maxScore = PlayerPrefs.GetInt("score");

        GameObject.FindGameObjectWithTag("maxScore").GetComponent<Text>().text = "Best score = " + maxScore;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

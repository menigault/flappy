﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeOffScreen : MonoBehaviour {

    public SpriteRenderer pipe;
    private Vector2 bl_corner, br_corner;

    private bool hasSpawnNew = false;
    private void Start()
    {
        bl_corner = Camera.main.ScreenToWorldPoint(new Vector2(0, 0));
        bl_corner = Camera.main.ScreenToWorldPoint(new Vector2(1, 0));

        float y_pos = Random.Range(1, 4) - 2;
        transform.position = new Vector3(transform.position.x, y_pos, transform.position.z);
    }
    // Update is called once per frame
    void Update () {
        if (pipe.transform.position.x + pipe.GetComponent<SpriteRenderer>().bounds.size.x / 2 < bl_corner.x)
        {
            Destroy(gameObject);
        }
        else if(!hasSpawnNew && pipe.transform.position.x + pipe.GetComponent<SpriteRenderer>().bounds.size.x / 2 < br_corner.x /2)
        {
            Instantiate((GameObject)Resources.Load("Pipe-pair"), transform.parent);
            hasSpawnNew = true;
        }
    }
}

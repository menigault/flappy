﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class over : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameObject.FindGameObjectWithTag("score").GetComponent<Text>().text = "Score = " + GameManager.instance.score;

        int maxScore = PlayerPrefs.GetInt("score"); 

        GameObject.FindGameObjectWithTag("maxScore").GetComponent<Text>().text = "Best score = " + maxScore;
    }
	
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class nextScene : MonoBehaviour {

    public string scenePath;
    public int time = 0;
	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        Invoke("load", time);
    }

    private void load()
    {
        SceneManager.LoadScene(scenePath, LoadSceneMode.Single);
    }
}

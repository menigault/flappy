﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tap2play : MonoBehaviour {

    public GameObject left, right, hand;
    private Vector3 lpos, rpos, handpos;

	// Use this for initialization
	void Start () {
        lpos = left.transform.position;
        rpos = right.transform.position;
        handpos = hand.transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        float t = Time.time*2;
        left.transform.position = lpos - new Vector3(Mathf.Sin(t), 0, 0);
        right.transform.position = rpos + new Vector3(Mathf.Sin(t), 0, 0);
        hand.transform.position = handpos - new Vector3(0, Mathf.Sin(t)/2, 0);
    }
}
